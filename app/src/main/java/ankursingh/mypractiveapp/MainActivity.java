package ankursingh.mypractiveapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView LoginTextView = null;
    TextView SignUpTextView = null;
    Button LoginButton = null;
    Button SignUpButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        SignUpTextView = findViewById(R.id.WelcomeMessage);
//        LoginTextView = findViewById(R.id.HelloWorld);
        LoginButton = findViewById(R.id.loginbutton);
        SignUpButton = findViewById(R.id.SignUp);

        LoginButton.setOnClickListener(this);
        SignUpButton.setOnClickListener(this);

        Log.d("tag","Inside OnCreate Function");

//        LoginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("tag","Somebody Pressed Login Button");
//                LoginTextView.setText("log in successful");
//            }
//        });
//
//        SignUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("tag","Somebody Pressed SignUp Button");
//                SignUpTextView.setText("SignUp successful");
//            }
//        });



    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("tag","Inside OnStart Function");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("tag","Inside OnResume Function");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("tag","Inside OnPause Function");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("tag","Inside OnStop Function");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("tag","Inside OnDestroy Function");
    }

    @Override
    public void onClick(View view) {
        setContentView(R.layout.activity_main);
        switch (view.getId())
        {
            case R.id.loginbutton:
                Log.d("tag","Somebody Pressed Login Button");
                LoginTextView = findViewById(R.id.HelloWorld);
                LoginTextView.setText("log in successful");
                break;

            case R.id.SignUp:
                Log.d("tag","Somebody Pressed Signup Button");
                SignUpTextView = findViewById(R.id.WelcomeMessage);
                SignUpTextView.setText("SignUp successful");
                break;
        }
    }
}
